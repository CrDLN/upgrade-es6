/*7.1 Dado el siguiente array, haz una suma de todos las notas de los examenes de 
los alumnos usando la función .reduce().*/

const exams = [
    {name: 'Yuyu Cabeza Crack', score: 5}, 
    {name: 'Maria Aranda Jimenez', score: 1}, 
    {name: 'Cristóbal Martínez Lorenzo', score: 6}, 
    {name: 'Mercedez Regrera Brito', score: 7},
    {name: 'Pamela Anderson', score: 3},
    {name: 'Enrique Perez Lijó', score: 6},
    {name: 'Pedro Benitez Pacheco', score: 8},
    {name: 'Ayumi Hamasaki', score: 4},
    {name: 'Robert Kiyosaki', score: 2},
    {name: 'Keanu Reeves', score: 10}
];

const sumScore = exams.reduce((acumulador,exam)=>{ return acumulador+exam.score},0);

console.log(sumScore);

/*7.2 Dado el mismo array, haz una suma de todos las notas de los examenes de los 
alumnos que esten aprobados usando la función .reduce().*/

const sumScoreAprob = exams.reduce((acumulador,exam)=>{ return (exam.score>5)?acumulador+exam.score:acumulador;},0);

console.log(sumScoreAprob);

/*7.3 Dado el mismo array, haz la media de las notas de todos los examenes .reduce().*/

const avgScore = exams.reduce((acumulador,exam,_, {length} )=>{ return acumulador+exam.score / length},0); //no he encontrado otra manera de hacerlo, pero no entiendo lo de _

console.log(avgScore);

/*Crea una arrow function que tenga dos parametros a y b y 
que por defecto el valor de a = 10 y de b = 5. Haz que la función muestre 
por consola la suma de los dos parametros.*/


//1.1 Ejecuta esta función sin pasar ningún parametro

const printConsoleNotParameters = () =>{
    const vA = 10;
    const vB = 5;
    console.log('Not parameters, a--> '+ vA + ' b--> '+ vB )
   };

printConsoleNotParameters();


//1.2 Ejecuta esta función pasando un solo parametro

let aAux = 10;

const printConsolOneParameter = (vA) =>{

    const vB = 5;
    console.log('One Parameter, a--> '+ vA + ' b--> '+ vB )
   };

printConsolOneParameter(aAux);


//1.3 Ejecuta esta función pasando dos parametros

let a = 10;
let b = 5;

const printConsoleTwoParameters = (vA,vB) =>{

    console.log('Two Parameters, a--> '+ vA + ' b--> '+ vB )
   };

printConsoleTwoParameters(a,b);